<?php

namespace Drupal\d_slack_message\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\d_application\Services\DropticaAPI;
use Drupal\d_user\Services\UserManager;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SlackMessaging for slack messages service.
 *
 * @package Drupal\d_slack_message\Services
 */
class SlackMessaging {

  use StringTranslationTrait;

  const SLACK_CHANNELS_ULR = "https://slack.com/api/conversations.list?pretty=1";

  const SLACK_MESSAGES_ULR = "https://slack.com/api/chat.postMessage";

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Http Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * KudosManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityTypeManager.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config factory.
   * @param \GuzzleHttp\Client $httpClient
   *   Http client.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Mail service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $database, ConfigFactoryInterface $config, Client $httpClient, RequestStack $request, LoggerChannelFactory $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->config = $config;
    $this->httpClient = $httpClient;
    $this->request = $request;
    $this->logger = $logger->get('d_slack_message');
  }

  /**
   * Send slack notification.
   *
   * @param object $slack_message_entity
   *   Slack message entity.
   */
  public function sendNotificationToSlack(object $slack_message_entity) {
    $headers = $this->prepareRequestHeader();
    $message = $slack_message_entity->get('message')->getValue()[0]['value'];
    $channels = $slack_message_entity->get('channel')->getValue();
    $webhook_url = self::SLACK_MESSAGES_ULR;
    foreach ($channels as $channel) {
      $message_options = $this->prepareMessageOptionsMessage($message, $channel['value']);
      $sending_data = json_encode($message_options);
      $this->postSlackMessage($headers, $webhook_url, $sending_data);
    }
  }

  /**
   * Send message to the Slack with more options.
   *
   * @param array $headers
   *   Slack message headers array.
   * @param string $webhook_url
   *   Slack webhook url.
   * @param string $sending_data
   *   String of data to send.
   *
   * @return \Psr\Http\Message\ResponseInterface|bool
   *   Can contain:
   *                          success    fail         fail
   *     - data:                ok       No hooks     Invalid channel specified
   *     - status message:      OK       Not found    Server Error
   *     - code:                200      404          500
   *     - error:               -        Not found    Server Error
   *
   * @throws \GuzzleHttp\Exception\GuzzleException;
   */
  private function postSlackMessage(array $headers = [], string $webhook_url = "", string $sending_data ) {
    try {
      $response = $this->httpClient->request('POST', $webhook_url, [
        'headers' => $headers,
        'body' => $sending_data,
      ]);
      $this->logger->info('Message was successfully sent!');
      return $response;
    }
    catch (ServerException $e) {
      $this->logger->error('Server error! It may appear if you try to use unexisting chatroom.');
      return FALSE;
    }
    catch (RequestException $e) {
      $this->logger->error('Request error! It may appear if you entered the invalid Webhook value.');
      return FALSE;
    }
  }

  /**
   * Function to request channels list using Slack Api webhook.
   *
   * @return array|bool
   *   Return slack channels list.
   */
  private function getSlackChannelsList() {
    $webhook_url = self::SLACK_CHANNELS_ULR;
    try {
      $response = $this->httpClient->request('GET', $webhook_url, [
        'headers' => $this->prepareRequestHeader(),
        'query' => ['limit' => 1000, 'pretty' => 1],
      ]);
      $res_body = json_decode($response->getBody(), TRUE);
      return $res_body['channels'];
    }
    catch (RequestException $e) {
      $this->logger->error('Request error! Can`t get channels list! It may appear if you entered the invalid Webhook value.');
      return FALSE;
    }
    catch (GuzzleException $e) {
      $this->logger->error($e);
      return FALSE;
    }
  }

  /**
   * Function to get slack channels list as array from request body object.
   *
   * @return array|bool
   *   Return slack channels list as array with data pairs id=>name.
   */
  public function getAllSlackChannelsArray() {
    $channels_list = [];
    if ($channels = $this->getSlackChannelsList()) {
      if (!empty($channels)) {
        foreach ($channels as $channel) {
          $channels_list[$channel['id']] = $channel['name'];
        }
      }
    }
    return $channels_list ?: FALSE;

  }

  /**
   * Function get slack channel name by channel id.
   *
   * @param string $channel_id
   *   Slack channel ID.
   *
   * @return array|bool
   *   Return channel name that match channel id.
   */
  public function getSlackChannelNameById(string $channel_id) {
    $channels = \Drupal::service('d_slack_message.slack_messaging')
      ->getSlackChannelsList();
    foreach ($channels as $channel) {
      if ($channel['id'] == $channel_id) {
        return $channel['name'];
      }
    }
    return FALSE;
  }

  /**
   * Prepare slack message body parameters.
   *
   * @param string $message
   *   Slack message content.
   * @param string $channel
   *   Slack channel.
   *
   * @return array
   *   Array of parameters.
   */
  private function prepareMessageOptionsMessage(string $message, string $channel): array {
    if ($message && $channel) {
      $message_options['text'] = $this->processMessage(strip_tags($message));
      $message_options['channel'] = "$channel";
    }
    $message_options["icon_url"] = "https://pbs.twimg.com/profile_images/950985062950035462/uv7kR3cP_400x400.jpg";
    return $message_options;
  }

  /**
   * Prepare request header parameters array.
   *
   * @return array
   *   Return array of request header elements.
   */
  private function prepareRequestHeader(): array {
    return [
      'Content-Type' => 'application/json',
      'Authorization' => 'Bearer ' . getenv('SLACK_BOT_TOKEN'),
    ];
  }

  /**
   * Preprocess message content.
   *
   * @param string $message
   *   Slack message content.
   *
   * @return array|string|string[]
   *   Return slack message to be send.
   */
  private function processMessage($message) {
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    if (preg_match_all("/$regexp/siU", $message, $matches, PREG_SET_ORDER)) {
      $i = 1;
      $links = [];
      foreach ($matches as $match) {
        $new_link = "<$match[2] | $match[3]>";
        $links['link-' . $i] = $new_link;
        $message = str_replace($match[0], 'link-' . $i, $message);
        $i++;
      }

      $message = strip_tags($message);

      foreach ($links as $id => $link) {
        $message = str_replace($id, $link, $message);
      }
    }
    return $message;
  }

  /**
   * Load slack message entity.
   *
   * @param int $entity_id
   *   Slack messsage entity id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSlackMessageEntity(int $entity_id) {
    try {
      return $this->entityTypeManager
        ->getStorage('d_slack_message')
        ->load($entity_id);
    }
    catch (RequestException $e) {
      $this->logger->error('Missing entity parameter.');
      return FALSE;
    }
  }

}
