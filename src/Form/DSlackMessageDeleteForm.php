<?php

namespace Drupal\d_slack_message\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\d_slack_message\Entity\DSlackMessage;
use Drupal\ultimate_cron\Entity\CronJob;

/**
 * Class DSlackMessageDeleteForm for creating delete form.
 *
 * @ingroup Drupal\d_kudos\Form
 */
class DSlackMessageDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * Gathers a confirmation question.
   */
  public function getQuestion() {
  }

  /**
   * Gathers a cancel url.
   */
  public function getCancelUrl() {
    return new Url('entity.d_slack_message.collection');
  }

  /**
   * The submit handler for the confirm form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $slack_message_id = $this->entity->id();
      DSlackMessage::load($slack_message_id)->delete();

      Cache::invalidateTags([
        'slack_message_list',
        'slack_message_#' . $slack_message_id,
      ]);
      if ($cron_job = CronJob::load('d_slack_message_cron_' . $slack_message_id)) {
        $cron_job->delete();
        $this->messenger()
          ->addMessage($this->t('Ultimate cron job id #slack_message_cron_@cid was deleted.', [
            '@cid' => $slack_message_id,
          ]));
      }
      $this->messenger()
        ->addMessage($this->t('Slack Message #@smid was deleted.', [
          '@smid' => $slack_message_id,
        ]));
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('d_slack_message')->error($e->getMessage());
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
