<?php

namespace Drupal\d_slack_message\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\d_slack_message\Services\SlackMessaging;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ultimate_cron\Entity\CronJob;

/**
 * Class Form controller for the d_slack_message entity edit forms.
 *
 * @package Drupal\d_slack_message\Form\DSlackMessageForm
 */
class DSlackMessageForm extends ContentEntityForm {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Slack messaging.
   *
   * @var \Drupal\d_slack_message\Services\SlackMessaging
   */
  protected $slackMessaging;

  /**
   * DSlackMessageForm constructor.
   *
   * @param \Drupal\d_slack_message\Services\SlackMessaging $slack_messaging
   *   Slack message messaging.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger Channel Factory.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   Entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   Time Interface.
   */
  public function __construct(
    SlackMessaging $slack_messaging,
    LoggerChannelFactory $logger,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    $this->slackMessaging = $slack_messaging;
    $this->logger = $logger->get('d_slack_message');
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('d_slack_message.slack_messaging'),
      $container->get('logger.factory'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.d_slack_message.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $entity = $this->entity;
      $date_time = new DrupalDateTime();
      if ($entity->isNew()) {
        $entity->set('created', $date_time->getTimestamp());
      }
      $entity->set('title', $form_state->getValue('title'));
      $entity->set('message', $form_state->getValue('message'));
      $entity->set('changes', $date_time->getTimestamp());
      $entity->set('cron_expression', $form_state->getValue('cron_expression'));
      $entity->set('channel', $form_state->getValue('channel'));
      $entity->save();

      // Check if ultimate cron entity config for this entity exists.
      if ($cron_config_entity = $this->loadUltimateCronEntityConfig($entity->id())) {
        $cron_config_entity->set('title', '[D_SLACK] ' . $entity->get('title')->getString());
        $cron_config_entity->save();
      }
      else {
        $values = [
          'title' => $entity->get('title')->getString(),
          'cron_expression' => $entity->get('cron_expression')->getString(),
          'id' => 'd_slack_message_cron_' . $entity->id(),
        ];
        $this->cronJobConfigCreate($values);
      }
    }
    catch (EntityStorageException $e) {
      $this->logger->error($e->getMessage());
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      watchdog_exception('slack', $e);
      return FALSE;
    }
  }

  /**
   * Function to create ultimate cron config on entity instance create.
   *
   * @param array $data
   *   Cron settings array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cronJobConfigCreate(array $data = []) {

    $entity_test = CronJob::create(
      [
        'title' => $data['title'],
        'id' => $data['id'],
        'module' => 'd_slack_message',
        'callback' => 'd_slack_message_slack_message_cron_callback',
        'scheduler' =>
          [
            'id' => 'crontab',
            'configuration' => [
              'rules' => [
                $data['cron_expression'],
              ],
              'catch_up' => 0,
            ],
          ],
      ]
    );
    $entity_test->save();
  }

  /**
   * Load ultimate cron entity config by slack message entity id.
   *
   * @param int $entity_id
   *   Cron Entity id.
   *
   * @return false|mixed
   *   Return cron entity or false if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadUltimateCronEntityConfig(int $entity_id) {
    return \Drupal::entityTypeManager()
      ->getStorage('ultimate_cron_job')
      ->load('d_slack_message_cron_' . $entity_id);
  }

}
