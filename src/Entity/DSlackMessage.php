<?php

namespace Drupal\d_slack_message\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines DSlackMessage entity.
 *
 * @ingroup d_slack_message
 *
 * @ContentEntityType(
 *   id = "d_slack_message",
 *   label = @Translation("DSlackMessage"),
 *   base_table = "d_slack_message",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "Title" = "title",
 *     "cron_expression" = "cron expression",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "message" = "message",
 *     "channel" = "channel",
 *   },
 *    handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\d_slack_message\Entity\Controller\DSlackMessageListBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\d_slack_message\Form\DSlackMessageForm",
 *       "edit" = "Drupal\d_slack_message\Form\DSlackMessageForm",
 *       "delete" = "Drupal\d_slack_message\Form\DSlackMessageDeleteForm",
 *     },
 *
 *   },
 *   admin_permission = "administer",
 *   links = {
 *     "canonnical" = "/slack_message/{d_slack_message}",
 *     "edit-form" = "/slack_message/{d_slack_message}/edit",
 *     "delete-form" = "/slack_message/{d_slack_message}/delete",
 *     "collection" = "/slack_message/list",
 *   },
 * )
 */
class DSlackMessage extends ContentEntityBase {

  use EntityChangedTrait;

  // Implements methods defined by EntityChangedInterface.

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('Slack message title'))
      ->setDisplayOptions('view', [
        'label' => 'Above',
        'type' => 'text_default',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -4,
      ])
      ->setRequired(TRUE);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('ID of the slack message entity'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID slack_message entity'))
      ->setReadOnly(TRUE);

    $fields['channel'] = BaseFieldDefinition::create("list_string")
      ->setLabel(t('Channel'))
      ->setSettings([
        'allowed_values_function' => 'd_slack_message_options_allowed_values',
      ])
      ->setDescription(t('Slack channel ID'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -2,
      ])
      ->setRequired(TRUE);

    $fields['cron_expression'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cron expression'))
      ->setDescription(t('Cron expresion eg. */15+@ * * * *'))
      ->setSettings([
        'default_value' => '*/15+@ * * * *',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => '-6',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time entity was created.'));

    $fields['changes'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changes'))
      ->setDescription(t('The time the entity was last time edited.'));

    $fields['message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message'))
      ->setDescription(t('Message content.'))
      ->setDisplayOptions('view', [
        'label' => 'Above',
        'type' => 'text_default',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -3,
        'format' => 'plain_text',
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
