
# Slack messaging module

Original author: marcin.debicki@droptica.pl [https://www.drupal.org/user/388301]


## CONTENTS OF THIS FILE

* OVERVIEW
* USAGE
* BEFORE INSTALL
* REQUIREMENTS
* INSTALLATION


## OVERVIEW

The Slack messaging module allows user (with granted permissions) to create slack messages that are sent at given time defined by cron expresion.

Permissions can be set for creating, editing, deleting, viewing and administrating slack messages.

# BEFORE INSTALL

Before install you need to create slack app, connect it with your selected slack workspace, configure it and get "bot token". Slack app can be created here: https://api.slack.com/apps (Create New App button). On )Auth & Permissions tab grant bot permissions to:
- channels:join
- channels:read
- chat:write
- chat:write.public
- groups:read

Add environment variable named SLACK_BOT_TOKEN and set its value to your bot token specific to your slack app.

## USAGE

Once Slack messaging module is installed, you can create content called Slack message. There couple of fields available - title, message body, channles (multiple choice) and crontab expresion. At saving Slack message content new cron job there is created with spefified crontab expresion.

Messages are posted at specified time on specified channels selected from public channles in your workspace.


## INSTALLATION

1) Copy all contents of this package to your modules directory preserving
   subdirectory structure.

2) Go to Administer -> Modules to install module.

3) Grant permissions.